package testing.platform.main.model;

import java.util.Collection;


public class Questions {
    private Collection<Question> easy;
    private Collection<Question> medium;
    private Collection<Question> hard;

    public Questions(Collection<Question> easy, Collection<Question> medium, Collection<Question> hard) {
        this.easy = easy;
        this.medium = medium;
        this.hard = hard;
    }

    public Integer getTotal() {
        return easy.size() + medium.size() + hard.size();
    }

    public Collection<Question> getEasy() {
        return easy;
    }

    public Collection<Question> getMedium() {
        return medium;
    }

    public Collection<Question> getHard() {
        return hard;
    }

    public Collection<Question> setLevelEasy(Collection<Question> easy){
        return this.easy = easy;
    }

    public Collection<Question> setLevelMedium(Collection<Question> medium){
        return this.medium = medium;
    }

    public Collection<Question> setLevelHard(Collection<Question> hard){
        return this.hard = hard;
    }


}
