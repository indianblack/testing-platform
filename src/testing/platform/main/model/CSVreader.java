package testing.platform.main.model;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

public class CSVreader {
    public static String line = "";
    public static String splitBy = "\\|";

    public static Level convertToEnum(String value) {
        try {
            return Level.valueOf(value);
        } catch (IllegalArgumentException iae) {
            System.err.println("The value passed did not match any of the original 'Level' enum values.");
            throw iae;
        }
    }

    public static Collection<String> collectAnswers(String a, String b, String c, String d) {
        Collection<String> allAnswers = new ArrayList<>();
        allAnswers.add(a);
        allAnswers.add(b);
        allAnswers.add(c);
        allAnswers.add(d);

        return allAnswers;
    }

    public static Collection<Question> getQuestions (ArrayList<Question> allQuestions, Level value) {
        return allQuestions.stream()
                .filter(it -> it.getLevel().equals(value))
                .collect(Collectors.toList());
    }

    public static Questions run() {
        ArrayList<Question> questions = new ArrayList<>();
        String filePath = "data/questions.csv";

        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            while ((line = reader.readLine()) != null) {
                String[] question = line.split(splitBy);
                questions.add(new Question(Integer.parseInt(question[0].trim()), question[1].trim(), question[2].trim(),convertToEnum(question[3].trim()), collectAnswers(question[4].trim(),question[5].trim(),question[6].trim(),question[7].trim())));
            }
        } catch (FileNotFoundException fnfe) {
            System.err.println("An attempt to open the file denoted by a specified pathname has failed");
        } catch (NumberFormatException nfe) {
            System.err.println("Error occurred while parsing question ordinal number");
        } catch (IOException ioe) {
            System.err.println("I/O error occurred while performing readLine()");
        }

        var allEasy = getQuestions(questions, Level.E);
        var allMedium = getQuestions(questions, Level.M);
        var allHard = getQuestions(questions, Level.H);

        return new Questions(allEasy, allMedium, allHard);
    }
}
