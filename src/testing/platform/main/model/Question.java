package testing.platform.main.model;

import java.util.Collection;

public class Question {
    private final Integer number;
    private final String content;
    private final String correctAnswer;
    private final Level level;
    private final Collection<String> answers;

    public Question(Integer number, String content, String correctAnswer, Level level, Collection<String> answers) {
        this.number = number;
        this.content = content;
        this.correctAnswer = correctAnswer;
        this.level = level;
        this.answers = answers;
    }

    public Level getLevel(){
        return level;
    }

    @Override
    public String toString() {
        StringBuilder allAnswers = new StringBuilder();
        if (answers.size() > 0) {
            for(String answer : answers) {
                allAnswers.append(answer + "\n");
            }
        } else {
            allAnswers.append("No answers were added.\n");
        }
        String ret = content + "\n" + allAnswers;
        return ret;
    }

}
