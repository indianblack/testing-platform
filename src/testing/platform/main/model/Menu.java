package testing.platform.main.model;

import java.util.Scanner;

public class Menu {

    public static void menuOptions () {
        System.out.println("Menu:\n" +
                "0 - Exit\n" +
                "1 - Prepare test\n" +
                "2 - Start test\n" +
                "3 - Show result pie chart\n");
    }

    public static void run () {
        // Using Scanner to get input from user
        Scanner input = new Scanner(System.in);

        // User input
        int userInput = -1;

        // Show menu
        menuOptions();
        System.out.println("Choose one of the options above: \n");

        while (userInput != 0) {
            try{
                userInput = input.nextInt();

                switch (userInput) {
                    case 0:
                        System.out.println("Bye!");
                        System.exit(0);
                        break;
                    case 1:
                        System.out.println("Prepare test.");
                        Test.prepare();
                        break;
                    case 2:
                        System.out.println("Run test.");
//                        Test.run();
                        break;
                    case 3:
                        System.out.println("Show results.");
//                        Test.result();
                        break;
                    default:
                        System.out.println("Option " + userInput + " is not available. Please choose one of the options below:\n");
                        menuOptions();
                }
            } catch (Exception exception) {
                System.out.println("Please provide one of the numbers available in the menu:\n");
                input.next();
            }
        }
    }

}
