package testing.platform.main.model;

public enum Level {
    E("Easy"),
    M("Medium"),
    H("Hard");

    private String description;

    Level(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
