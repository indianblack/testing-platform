package testing.platform.main.model;

import java.util.Scanner;

public class Test {
    public static final Questions questions = CSVreader.run();

    public static void prepare() {
        int questionsInTotal = 0;
        int easyQuestions = 0;
        int mediumQuestions = 0;
        int hardQuestions = 0;
        int time = 0;

        int total = questions.getTotal();
        int easy = questions.getEasy().size();
        int medium = questions.getMedium().size();
        int hard = questions.getHard().size();

        try {
            Scanner input = new Scanner(System.in);

            System.out.println("Enter total number of questions. The number should be smaller or equal " + total + ":");
            questionsInTotal = input.nextInt();
            if (questionsInTotal > total) {
                System.out.println("Provided number exceeds number of questions form CSV file.");
                System.out.println("Number should not exceed " + total + " Try again: ");
                questionsInTotal = input.nextInt();
            }

            System.out.println("Enter the number of questions on easy difficulty level. The number should be smaller or equal " + easy + ":");
            easyQuestions = input.nextInt();
            if (easyQuestions > easy) {
                System.out.println("Provided number exceeds number of questions form CSV file.");
                System.out.println("Number should not exceed " + easy + " Try again: ");
                easyQuestions = input.nextInt();
            }

            System.out.println("Enter the number of questions on medium difficulty level. The number should be smaller or equal " + medium + ":");
            mediumQuestions = input.nextInt();
            if (mediumQuestions > medium) {
                System.out.println("Provided number exceeds number of questions form CSV file.");
                System.out.println("Number should not exceed " + medium + " Try again: ");
                mediumQuestions = input.nextInt();
            }

            System.out.println("Enter the number of questions on hard difficulty level. The number should be smaller or equal " + hard + ":");
            hardQuestions = input.nextInt();
            if (hardQuestions > hard) {
                System.out.println("Provided number exceeds number of questions form CSV file.");
                System.out.println("Number should not exceed " + hard + " Try again: ");
                hardQuestions = input.nextInt();
            }

            System.out.println("How much time should the test take? Enter the number of minutes: ");
            time = input.nextInt();

        } catch (Exception exception) {
                System.out.println("Please provide a number.");
                prepare();
            }

        if (questionsInTotal != 0 && questionsInTotal != easyQuestions + mediumQuestions + hardQuestions) {
            System.out.println("The sum of the questions from each level doesn't match the total number of questions in the test. Please try again.");
            prepare();
        }

        System.out.println("Total number of questions: " + questionsInTotal + "\n");
        System.out.println("Number of questions from level easy: " + easyQuestions + "\n");
        System.out.println("Number of questions from level medium: " + mediumQuestions + "\n");
        System.out.println("Number of questions from level hard: " + hardQuestions + "\n");
        System.out.println("Time in minutes: " + time + "\n");
    }
}
