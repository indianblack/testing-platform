# Testing platform

**Description:** A platform with tests that allows recruiter to check the level of a developer during an interview.

**Class:** Object-oriented programming in Java

## Basic information

### Basic requirements
1. The questions will be saved in a csv file.
2. The questions will be edited manually directly in the csv file.
3. The test result should be displayed at the end of the test.
4. The questions should have 3 levels: easy, medium, hard.
5. It should be possible to define the number of questions.
6. It should be possible to define the number of questions for each difficulty level.
7. The total number of questions should be consistent with the total number of questions.
8. This should be a console application, but at the end a pie chart should be displayed showing the number of correct and incorrect questions (use: [JFreeChart](https://www.jfree.org/jfreechart/)).
9. The test should end after a certain time.
10. Stop the test when the time runs out.
11. Application should be written in English.


### CSV file
CSV file with questions should contain:
- ordinal number
- content of the question
- correct answer
- 4 possible answers, one of which is correct
- level (easy, medium, hard)

## Instructions
1. Clone this repository.
2. Run project in IDE.

## Useful links
- https://www.softwaretestinghelp.com/core-java-interview-questions/
- https://www.javacodeexamples.com/java-split-string-by-pipe-example/652

## Licencja
[MIT](https://choosealicense.com/licenses/mit/)